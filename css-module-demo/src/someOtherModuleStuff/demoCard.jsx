import React from 'react';

import {card, small} from './card.module.css';

export default ({isSmall=false, children}) => (
    <div className={`${isSmall ? small : card}` }> {children} </div>
)