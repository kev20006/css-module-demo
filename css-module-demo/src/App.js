import React from 'react';

import Button from './button/Button';
import ThreeDButton from './scoped-button/3d-Button';
import BothButton from './both-button/Both-button';
import Card from './someOtherModuleStuff/demoCard';

import './App.css';

const App = () => (
  <>
    <h1>Scoped CSS Demo</h1>
    <Button onClick={()=>alert('click')}>My Button</Button>
    {/*<ThreeDButton onClick={()=>console.log('bleh')}> My Other Button</ThreeDButton>*/}
    {/*<BothButton>Third Button</BothButton>*/}
    {/*<Card><h1 className='green'>I am Card</h1></Card>*/}
    {/*<Card isSmall>I'm a little card</Card>*/}
  </>
);

export default App;
