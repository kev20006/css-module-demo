import React from 'react'

import styles from './style.module.css';

export default ({children, ...args}) => (
    <div className={styles.buttonWrapper}>
        <div className={styles.buttonLower} />
        <button className={styles.button} {...args}>{children}</button>
        <div className={styles.buttonUpper} />
    </div>
);  