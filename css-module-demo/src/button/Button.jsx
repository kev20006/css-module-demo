import React from 'react'

export default ({children, ...args}) => (
    <button className="button" {...args}>{children}</button>
);