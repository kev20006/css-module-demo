import React from 'react'

import styles from './both.module.css';

export default ({children, ...args}) => (
    <button className={`${styles.button} button`} {...args}>{children}</button>
);